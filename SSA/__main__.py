import os, time, shutil, sys, glob, logging, subprocess
from SSA import argparse
from pubchempy import Compound, get_compounds
from rdkit.Chem import AllChem
from rdkit import Chem
from rdkit.Chem.inchi import MolToInchi

def parsing_RC(path):
    rc_list = []
    with open(path) as fr:
        lines = fr.read().splitlines()
        for idx, val in enumerate(lines):
            if 'FINGERPRINTS RC' in val:
                RC = lines[idx+1]
                t_RC = RC[1:-1].split(',')
                new_t_RC = []
                for t_c in t_RC:
                    t = t_c.split(':')[0].strip()
                    new_t_RC.append(t)
                rc_list = new_t_RC
    return rc_list


def RDT(predicted_precursors):
    RDT_path = './data/RDT1.5.1.jar'
    rdt_results = {}
    for rule_num, precursor_smi, target_smi in predicted_precursors:
        subprocess.call(['java', '-jar', RDT_path, '-Q', 'SMI', '-q', '"' + precursor_smi + '>>' + target_smi + '"', '-g', '-j','ANNOTATE', '-f', 'TEXT', '-x'])
        rdt_result_file = './ECBLAST_smiles_ANNONATE.txt'
        if os.path.exists(rdt_result_file):
            RC_list =  parsing_RC(rdt_result_file)
            rdt_results[precursor_smi+'>>'+target_smi] = RC_list
        rm_files = glob.glob('./ECBLAST_smiles_ANNONATE*')
        for f in rm_files:
            os.remove(f)
    return rdt_results


def classify_molecule_type(predicted_precursors):
    molecule_type = {}
    mesh_dict = {}
    with open('./data/supp2020_mesh_parsed.txt') as fr:
        lines = fr.read().splitlines()
        for line in lines:
            tok = line.split('\t')
            num = tok[0]
            descriptor = tok[1]
            mesh_dict[num] = descriptor

    for rule_num, precursor_smi, target_smi in predicted_precursors:
        try:
            pre_inchi = MolToInchi(Chem.MolFromSmiles(precursor_smi))
            cmpd = get_compounds(pre_inchi, 'inchi')
            print(cmpd)
        except:
            print('pubchem error: get_compounds()')
            continue
        mol_type = []
        iupacName = cmpd[0].iupac_name
        if iupacName:
            mol_type.append(iupacName)
        synon = cmpd[0].synonyms
        if synon:
            for syn_name in synon:
                if syn_name in mesh_dict.keys():
                    mesh_tok = mesh_dict[syn_name].split('!#')
                    for i in mesh_tok:
                        mol_type.append(i)
        molecule_type[precursor_smi] = mol_type
    return molecule_type


def predict_precursors(reaction_rule_set, target_mol):
    precursors = set()
    try:
        for rule_num in reaction_rule_set.keys():
            rxn_rule = reaction_rule_set[rule_num]
            reactant_rule = rxn_rule.split('>>')[0]
            product_rule = rxn_rule.split('>>')[1]
            rxn = AllChem.ReactionFromSmarts(product_rule+'>>'+reactant_rule)
            ps = rxn.RunReactants((target_mol,))
            if len(ps) > 0:
                for pro in ps:
                    for each_product in ps:
                        precur_smiles = Chem.MolToSmiles(each_product[0])
                        target_smiles = Chem.MolToSmiles(target_mol)
                        precursors.add((rule_num, precur_smiles, target_smiles))
    except:
        pass
    return precursors

def get_reaction_rule_set():
    reaction_rule_set = {}
    with open('./reaction_rules/reaction_rules.txt') as fr:
        lines = fr.read().splitlines()
        for li in lines:
            rule_num = li.split()[0]
            rxn_rule = li.split()[1]
            reaction_rule_set[rule_num] = rxn_rule
    return reaction_rule_set


def main():
    start = time.time()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    parser = argparse.argument_parser()
    options = parser.parse_args()

    target_smiles = options.input_smiles
    output_dir = options.output_dir

    try:
        os.mkdir(output_dir)
    except:
        rm_files = glob.glob(output_dir+'/*')
        for rf in rm_files:
            os.remove(rf)
        pass

    ### Predict precursors using retrobiosynthesis
    reaction_rule_set = get_reaction_rule_set()
    target_mol = Chem.MolFromSmiles(target_smiles)
    target_mol_H = Chem.AddHs(target_mol)
    predicted_precursors = predict_precursors(reaction_rule_set, target_mol_H)
    with open(output_dir+'/predicted_precursors.txt', 'w') as fw:
        fw.write('Reaction_rule_ID\tPredicted_precursors\tTarget_products\n')
        for rule_num, precursor, target_product in predicted_precursors:
            fw.write('%s\t%s\t%s\n' %(rule_num, precursor, target_product))
    
    ### Select precursors having a suitable molecule type
    molecule_type = classify_molecule_type(predicted_precursors)
    with open(output_dir+'/molecule_type.txt', 'w') as fw:
        fw.write('Predicted_precursors\tMolecule_types\n')
        for precursor in molecule_type.keys():
            mol_type_str = '; '.join(molecule_type[precursor])
            fw.write('%s\t%s\n' %(precursor, mol_type_str))

    ### Select precursors having a consistent relative position of functional groups using RDT tool
    rdt_list = RDT(predicted_precursors)
    with open(output_dir+'/reaction_center.txt', 'w') as fw:
        fw.write('Predicted_precursors\tTarget_products\tReaction_centers\n')
        for rxn in rdt_list.keys():
            prec_smi = rxn.split('>>')[0]
            targ_smi = rxn.split('>>')[1]
            rc_list = rdt_list[rxn]
            rc_list_str = ', '.join(rc_list)
            fw.write('%s\t%s\t%s\n' %(prec_smi, targ_smi, rc_list_str))

    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))


if __name__ == '__main__':
    main()

