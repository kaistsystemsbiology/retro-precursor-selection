import argparse

def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_smiles', required=True,  help='SMILES of target product for retrobiosynthesis')
    parser.add_argument('-o', '--output_dir', required=True, help='Output directory')
    return parser
